import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {DiaryCategory} from "./DiaryCategory";

@Entity()
export class DiaryCategoryField {
    @PrimaryGeneratedColumn()
    id: number;


    @Column()
    fieldName: string;

    @Column()
    value: string;

    @ManyToOne(type=> DiaryCategory, diaryCategory => diaryCategory.fields )
    diaryCategory: DiaryCategory;
}
