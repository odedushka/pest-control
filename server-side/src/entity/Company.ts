import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";
import {Pest} from "./Pest";

@Entity()
export class Company {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    companyProfile: string;

    @OneToOne(type => Pest)
    @JoinColumn()
    admin: Pest

}
