import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {DiaryCategory} from "./DiaryCategory";

@Entity()
export class DiaryCategoryAction {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    action: string;

    @ManyToOne(type=> DiaryCategory, diaryCategory => diaryCategory.actions )
    diaryCategory: DiaryCategory;


}
