import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Remedy {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    concentrateMaterial: string;

    @Column()
    concentratePerfume: string;

    @Column()
    batch: string;

    @Column()
    activeIngredient: string;

    @Column()
    manufacture: string;

}
