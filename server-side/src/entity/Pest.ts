import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Pest {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    phone: string;

    @Column()
    mail: string;

    @Column()
    address: string;

    @Column()
    licenseNumber: string;

    @Column()
    licenseExpire: string;

    @Column({
        default: 0
    })
    isCompanyAdmin: boolean;

    @Column({
        default: 0
    })
    isAdmin: boolean;

    @Column()
    signature:string;


    @Column()
    avatar:string;

}
