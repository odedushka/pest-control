import {Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Customer} from "./Customer";

@Entity()
export class DiaryLocation {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    address: string;

    @Column()
    stopsWithoutPoison: number;
    @Column()
    stopsWithPoison: number;
    @Column()
    entomologistAmount: number;

}
