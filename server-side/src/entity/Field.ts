import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Category} from "./Category";

@Entity()
export class Field{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name: string;


    @ManyToOne(type=> Category, category=> category.fields)
    category:Category;
}
