import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Diary} from "./Diary";

@Entity()
export class DiaryRemedy {
    @PrimaryGeneratedColumn()
    id: number;


    @ManyToOne(type=> Diary, diary => diary.remedies)
    diary:Diary;
}
