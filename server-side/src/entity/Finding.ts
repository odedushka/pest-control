import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Finding {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 2000
    })
    value: string;

}
