import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Location} from "./Location";

@Entity()
export class Customer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    phone: string;

    @Column()
    mail: string;

    @Column()
    address: string;

    @Column()
    postalCode: string;

    @OneToMany(type=> Location, location => location.customer)
    locations: Location[];
}
