import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Description {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 2000
    })
    value: string;

}
