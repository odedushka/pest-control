import {Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Diary} from "./Diary";
import {Category} from "./Category";
import {DiaryCategoryAction} from "./DiaryCategoryAction";
import {DiaryCategoryRow} from "./DiaryCategoryRow";
import {DiaryCategoryField} from "./DiaryCategoryField";

@Entity()
export class DiaryCategory{
    @PrimaryGeneratedColumn()
    id:number;

    @ManyToOne(type=> Diary, diary=> diary.diaryCategories, {primary:true})
    diary: Diary;

    @ManyToOne(type=> Category, category=> category.categoryDiaries, {primary:true})
    category: Category;

    @OneToMany(type=> DiaryCategoryAction, diaryCategoryAction => diaryCategoryAction.diaryCategory )
    actions: DiaryCategoryAction[];


    @OneToMany(type=> DiaryCategoryRow, diaryCategoryRow => diaryCategoryRow.diaryCategory )
    rows: DiaryCategoryRow[];

    @OneToMany(type=> DiaryCategoryField, diaryCategoryField => diaryCategoryField.diaryCategory )
    fields: DiaryCategoryField[];



}


