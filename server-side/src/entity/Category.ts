import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {DefaultAction} from "./DefaultAction";
import {Field} from "./Field";
import {DiaryCategory} from "./DiaryCategory";

@Entity()
export class Category {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    rowName: string;

    @OneToMany(type => DefaultAction, defualtAction => defualtAction.category)
    defaultActions: DefaultAction[];

    @OneToMany(type => Field, field => field.category)
    fields: Field[];


    @OneToMany(type => DiaryCategory, diaryCategory=> diaryCategory.category)
    categoryDiaries: DiaryCategory[];
}
