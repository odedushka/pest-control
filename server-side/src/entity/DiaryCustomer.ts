import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Location} from "./Location";
import {DiaryLocation} from "./DiaryLocation";

@Entity()
export class DiaryCustomer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    phone: string;

    @Column()
    mail: string;

    @Column()
    address: string;

    @Column()
    postalCode: string;

    @OneToOne(type => Location,{cascade:true})
    @JoinColumn()
    location: DiaryLocation;
}
