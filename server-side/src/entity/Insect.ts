import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Insect {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

}
