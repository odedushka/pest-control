import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Customer} from "./Customer";

@Entity()
export class Location {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    address: string;

    @ManyToOne(type => Customer, customer => customer.locations)
    customer:Customer;

}
