import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {DiaryCategory} from "./DiaryCategory";

@Entity()
export class DiaryCategoryRow {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    key: string;

    @Column()
    value: string;

    @ManyToOne(type=> DiaryCategory, diaryCategory => diaryCategory.rows )
    diaryCategory: DiaryCategory;
}
