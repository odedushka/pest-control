import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Diary} from "./Diary";

@Entity()
export class DiaryProduct{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name: string;

    @Column()
    amount: number;

    @Column({default: 0})
    isCharge: boolean;

    @Column()
    chargeAmount: number;

    @Column({default: 0})
    isVat: boolean;

    @ManyToOne(type=> Diary, diary => diary.remedies)
    diary:Diary;

}
