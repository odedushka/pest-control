import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {DiaryInsect} from "./DiaryInsect";
import {Remedy} from "./Remedy";
import {DiaryRemedy} from "./DiaryRemedy";
import {DiaryProduct} from "./DiaryProduct";
import {Category} from "./Category";
import {DiaryCategory} from "./DiaryCategory";

@Entity()
export class Diary {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 2000
    })
    placeDescription: string;

    @Column({
        length: 2000
    })
    monitoringFindings: string;

    @Column({
        default: 0
    })
    irregular: boolean;

    @Column()
    warranty: string;


    @Column({
        length: 2000
    })
    notesForCustomer: string;

    @Column()
    additionalMails: string;

    @Column()
    signer: string;

    @Column()
    signerSignature: string;


    @Column()
    branchSignature: string;

    @Column()
    pestSignature: string;


    @Column({
        default: 1
    })
    isDraft: boolean;

    @OneToMany(type => DiaryInsect, diaryInsect => diaryInsect.diary)
    insects: DiaryInsect[];

    @OneToMany(type => DiaryRemedy, diaryRemedy => diaryRemedy.diary)
    remedies: DiaryRemedy[];

    @OneToMany(type => DiaryProduct, diaryProduct => diaryProduct.diary)
    products: DiaryProduct[];

    @OneToMany(type => DiaryCategory, diaryCategory=> diaryCategory.diary)
    diaryCategories: DiaryCategory[];

}
