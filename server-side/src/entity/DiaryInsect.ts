import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Diary} from "./Diary";

@Entity()
export class DiaryInsect {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    insectName:string;


    @Column()
    infectionLevel: string;


    @Column()
    actionsProvided: string;

    @ManyToOne(type=> Diary, diary => diary.insects)
    diary:Diary;
}
